<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>alert_failed</name>
   <tag></tag>
   <elementGuidId>090a109d-74b6-4110-a997-0b601acf2149</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//p[@class='lead text-danger']</value>
      </entry>
      <entry>
         <key>BASIC</key>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
